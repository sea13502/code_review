# 正常的流程
- 事例代码仓库 http://git.oschina.net/sea13502/code_review
![](http://git.oschina.net/sea13502/code_review/raw/master/img/1.png)
- 这样就可以避免误提交到此分支，日常开发提交到dev分支，待测试完毕，没有问题，可以提交到master分支如果普通开发者直接将代码push到主master分支，则会报错
- 现在场景是，我是这个项目的管理者，然后有两个开发者，小茹和小丽
- 首先来一次交叉审核，小茹提的pull request由小丽来审核，小丽的由小茹来审核。（冲突的情况后面要考虑一下）
![](http://git.oschina.net/sea13502/code_review/raw/master/img/2.png)
- 作为项目管理者的我可以看到这两个pr（pull request 后面会采用pr这个简写），其他所有的管理者和开发者都可以在自己的时间线中，看到这条提示，并且参与讨论。
- 进入小丽的pr，指派审查人员和测试人员为小茹，然后点保存，然后进入小茹的pr，指派审查和测试为小丽和hai，只要有hai和小丽其中一人审核此代码pr通过，管理员就可以将小茹的代码合并到dev开发分支中。
![](http://git.oschina.net/sea13502/code_review/raw/master/img/3.png)
![](http://git.oschina.net/sea13502/code_review/raw/master/img/4.png)
![](http://git.oschina.net/sea13502/code_review/raw/master/img/5.png)
被指派的人进入这个人需要审核的pr中，都会有审核和测试通过按钮，有问题可以在下面的代码处，添加自己的评论，只有当审查人或测试人点击审查和测试通过这个按钮后，代码就能合并到对应的分支中。